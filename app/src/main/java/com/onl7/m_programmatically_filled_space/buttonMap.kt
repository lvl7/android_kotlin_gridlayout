package com.onl7.m_programmatically_filled_space

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.util.Log
import android.widget.Button
import android.widget.GridLayout

class ButtonMap : GridLayout {
    val TAG = "ButtonMap"

    // Lets say we want to explicit number of columns and add as many squared buttons to fill whole layout
    val COLUMNS = 10


    // This constructions are needed for GridLayout ---
    // I think any time when you inherit from View, simply past them
    // https://medium.com/@rygel/stop-repeating-yourself-and-create-custom-views-on-android-with-kotlin-f5b3fc581c0e
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }
    // This constructions are needed for GridLayout ===


    // This is out function that are called when view is created
    fun init(context: Context) {
        Log.d(TAG, "layout created")

        setButtons()
    }

    fun setButtons(){
        // when view is created, size is not set already.
        Log.d(TAG, "Now width: ${this.width}")

        // se we need to check it after is set -- for example by adding task to its queue:
        this.post(java.lang.Runnable {
            Log.d(TAG, "But now width: ${this.width}")
            // set numbers of columns
            this.columnCount = COLUMNS

            // calculated but not used to set button Width, see bellow
            val buttonHeight = this.width / COLUMNS
            val buttonsInColumn = this.height / buttonHeight -1

            for (index in 1..(COLUMNS * buttonsInColumn)) {
                val button = Button(context)
                button.text = index.toString()
                button.textSize = 10f

                // sooo For this long hours I tried put buttons in such way that they will be that
                // size to fill screen. So if you need such behaviour, we need to set weight of
                // each element to same value.

                // But at the end of a day, when we have width of parent (to calculate height of
                // buttons to make them square) we can us it to set width of button as well..
                // But I'll keep this solution with weight to cover more educational field :P
                val params = GridLayout.LayoutParams(
                        // width of button
                        GridLayout.spec(GridLayout.UNDEFINED, buttonHeight),
                        // last parameter is weight, so each button will have 1
                        GridLayout.spec(GridLayout.UNDEFINED, GridLayout.FILL, 1f))


                params.width = 0

                button.layoutParams = params

                button.setOnClickListener({
                    Log.d(TAG, "I am button on coords: ${it.x} : ${it.y}")
                })
                this.addView(button)
            }
        })
    }
}